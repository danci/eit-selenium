# EIT Selenium assignment
## Report sheet
[Click here for the Google spreadsheet](https://docs.google.com/spreadsheets/d/1kleM83bozipeqXrSGBGzEPURpr06pzbdCXN0xSxddCE/edit#gid=0)
## Video about the test
[![Video about the Selenium test](https://img.youtube.com/vi/1ZlV-w0_Hok/0.jpg)](https://www.youtube.com/watch?v=1ZlV-w0_Hok)
## Comments
The captcha has to be solved manually. Everything else is automatic.