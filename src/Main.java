import static org.junit.Assert.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import kointesting.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {
    static WebDriver koinDriver;
    static WebDriver mailDriver;
    static String email = "aaazzz699@1.fackme.gq";
    static String password = "titkos";

    @BeforeClass
    public static void initializeTest(){
        System.setProperty("webdriver.chrome.driver", "d:/packages/chrome/chromedriver.exe");
        koinDriver = new ChromeDriver();
        mailDriver = new ChromeDriver();

        EmalPage emalPage = new EmalPage(mailDriver);
        RegisterPage registerPage = new RegisterPage(koinDriver);

        email = emalPage.getEmailAddress();
        registerPage.registerWithEmail(email,password);
        emalPage.validateEmail();

        logOut();
        logIn();
    }

    @AfterClass
    public static void closeTest(){
        logOut();

        koinDriver.close();
        mailDriver.close();
    }

    private static void logIn() {
        SignInPage signInPage = new SignInPage(koinDriver);
        signInPage.login(email,password);
        assertTrue(signInPage.isFinished());
    }

    private static void logOut() {
        LogoutPage logoutPage = new LogoutPage(koinDriver);
        logoutPage.logout();
        assertTrue(logoutPage.isFinished());
    }

    @Test
    public void createAndCheckNewItem() throws InterruptedException {
        Random rand = new Random();
        Integer  price = rand.nextInt(998) + 1;
        String name = "sajtreszelo" + price.toString();

        ItemConstructionCheckingPage itemConstructionCheckingPage = new ItemConstructionCheckingPage(koinDriver);
        assertTrue(itemConstructionCheckingPage.createAndCheckNewItem(name,price));
    }

    @Test
    public void testStaticPages(){
        List<StaticPageObject> staticPageObjectList;
        staticPageObjectList = loadStaticPagesFromFile("static-pages.json");

        for (int i = 0; i< staticPageObjectList.size(); i++){
            staticPageObjectList.get(i).performStaticPageTest();
        }
    }

    private List<StaticPageObject> loadStaticPagesFromFile(String fileName){
        String jsonContent = null;
        try {
            jsonContent = new Scanner(new File(fileName)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        JsonParser parser = new JsonParser();
        JsonArray json = (JsonArray) parser.parse(jsonContent);
        return loadStaticPagesFromJson(json);
    }

    private List<StaticPageObject> loadStaticPagesFromJson(JsonArray json){
        List<StaticPageObject> result = new ArrayList<>();
        for (int i=0; i<json.size(); i++){
            result.add(loadStaticPageFromJson((JsonObject) json.get(i)));
        }
        return result;
    }
    private StaticPageObject loadStaticPageFromJson(JsonObject json){
        String url = Routines.trimString(json.get("url").toString());
        StaticPageObject page = new StaticPageObject(koinDriver,url);
        System.out.println("JSON page: \""+url+"\"");
        JsonArray elements = (JsonArray) json.get("elements");
        for (int i=0; i<elements.size(); i++){
            JsonObject element = (JsonObject) elements.get(i);
            String xpath = Routines.trimString(element.get("xpath").toString());
            String property = Routines.trimString(element.get("property").toString());
            String expectedValue = Routines.trimString(element.get("expectedValue").toString());
            System.out.println(
                    "xpath=\"" + xpath + "\" "+
                    "property=\"" + property + "\" "+
                    "expectedValue=\"" + expectedValue + "\""
            );
            page.addElementToTest(xpath,property,expectedValue);
        }
        return page;
    }
}