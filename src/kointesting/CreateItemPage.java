package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

/**
 * Created by Rátai Dániel on 2017. 05. 26..
 */
public class CreateItemPage extends PageObject{
    public CreateItemPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(className = "select2-choice")
    private WebElement categoryDropdown;

    @FindBy(id = "transactioncreate-value")
    private WebElement itemPrice;

    @FindBy(id = "s2id_autogen3")
    private WebElement itemTags;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnSubmitItem;

    private By categoryItem = By.className("select2-result-selectable");

    public void setCategory(){
        actionBot.click(categoryDropdown);
        actionBot.click(categoryItem);
    }

    public void setPrice(Integer price){
        actionBot.retype(itemPrice,price.toString());
    }

    public void addTag(String tag){
        actionBot.type(itemTags,tag);
        actionBot.type(itemTags, Keys.RETURN);
        actionBot.submit(itemTags);
    }

    public void submitItem(){
        actionBot.click(btnSubmitItem);
    }

    public void createItem(String name, Integer price){
        setCategory();
        setPrice(price);
        addTag(name);
        submitItem();
    }
}
