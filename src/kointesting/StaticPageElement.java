package kointesting;

/**
 * Created by Rátai Dániel on 2017. 05. 30..
 */
public class StaticPageElement {
    private String xpath;
    private String property;
    private String expectedValue;

    public StaticPageElement(String xpath, String property, String expectedValue){
        this.xpath = xpath;
        this.property = property;
        this.expectedValue = expectedValue;
    }

    public String getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(String expectedValue) {
        this.expectedValue = expectedValue;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public String toString(){
        return  "xpath=\"" + xpath + "\" "+
                "property=\"" + property + "\" "+
                "expectedValue=\"" + expectedValue + "\"";
    }
}
