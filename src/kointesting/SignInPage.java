package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rátai Dániel on 2017. 05. 26..
 */
public class SignInPage extends PageObject {
    @FindBy(id="loginform-email")
    private WebElement email;

    @FindBy(id="loginform-pass")
    private WebElement password;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public void login(String email, String password){
        actionBot.navigateToUrl("https://www.koin.hu/");
        assertEquals("Koin - költségkövető alkalmazás",actionBot.getActualPageTitle());
        actionBot.retype(this.email,email);
        actionBot.retype(this.password,password);
        actionBot.submit(this.password);
    }

    public boolean isFinished(){
        return actionBot.waitUntilVisible(By.className("login-info__text"));
    }
}
