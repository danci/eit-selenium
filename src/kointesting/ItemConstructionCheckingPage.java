package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Rátai Dániel on 2017. 05. 26..
 */
public class ItemConstructionCheckingPage extends PageObject {
    public ItemConstructionCheckingPage(WebDriver driver) {
        super(driver);
    }

    public boolean createAndCheckNewItem(String name, Integer price){
        actionBot.navigateToUrl("https://www.koin.hu/main");
        CreateItemPage createItemPage = new CreateItemPage(driver);
        createItemPage.createItem(name,price);

        CheckLastItemPage checkLastItemPage = new CheckLastItemPage(driver);
        checkLastItemPage.waitForLastItemToBe(name);

        return  (name.equals(checkLastItemPage.getLastItemName()) &&
                (price.toString()+" Ft").equals(checkLastItemPage.getLastItemPrice()));
    }
}
