package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rátai Dániel on 2017. 05. 26..
 */
public class CheckLastItemPage extends PageObject {
    public CheckLastItemPage(WebDriver driver) {
        super(driver);
    }

    private By lastItemName = By.xpath("//span[@class='label label-default']");
    private By lastItemPrice = By.xpath("//div[@class='transaction-value']");

    public void waitForLastItemToBe(String name){
        actionBot.waitUntilTextEquals(lastItemName,name);
    }

    public String getLastItemName(){
        return actionBot.getText(lastItemName);
    }

    public String getLastItemPrice(){
        return actionBot.getText(lastItemPrice);
    }
}
