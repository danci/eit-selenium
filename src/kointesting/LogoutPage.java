package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Rátai Dániel on 2017. 05. 26..
 */
public class LogoutPage extends PageObject {
    @FindBy(xpath = "//a[@href='/site/logout']")
    private WebElement btnLogout;

    public LogoutPage(WebDriver driver) {
        super(driver);
    }

    public void logout() {
        actionBot.click(btnLogout);
    }

    public boolean isFinished(){
        return actionBot.waitUntilVisible(By.className("btn-facebook"));
    }
}
