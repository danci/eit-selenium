package kointesting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageObject {
    protected WebDriver driver;
    protected ActionBot actionBot;


    public PageObject(WebDriver driver){
        this.driver = driver;
        actionBot = new ActionBot(driver);
        PageFactory.initElements(driver, this);
    }
}