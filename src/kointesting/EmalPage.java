package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Rátai Dániel on 2017. 05. 31..
 */
public class EmalPage extends PageObject {

    public EmalPage(WebDriver driver) {
        super(driver);
    }

    public String getEmailAddress() {
        actionBot.navigateToUrl("https://emailfake.com/");
        return actionBot.getText(By.id("email_ch_text"));
    }

    public void validateEmail(){
        actionBot.click(By.xpath("//div[@class='mess_bodiyy']/a[2]"));
    }
}
