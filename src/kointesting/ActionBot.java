package kointesting;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

import java.util.concurrent.TimeUnit;


import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by Rátai Dániel on 2017. 05. 26..
 */
public class ActionBot {
    private final WebDriver driver;
    Wait<WebDriver> wait;

    public ActionBot(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }


    public void click(WebElement element){
        element.click();
    }

    public void click(By locator) {
        waitUntilVisible(locator);
        click(driver.findElement(locator));
    }

    public void retype(WebElement element, CharSequence... text){
        element.clear();
        type(element,text);
    }

    public void retype(By locator, CharSequence... text) {
        if (waitUntilVisible(locator)){
            retype(driver.findElement(locator),text);
        }
    }

    public void type(WebElement element, CharSequence... text){
        element.sendKeys(text);
    }

    public void type(By locator, CharSequence... text) {
        if (waitUntilVisible(locator)) {
            type(driver.findElement(locator), text);
        }
    }

    public void submit(WebElement element){
        element.submit();
    }

    public void submit(By locator) {
        submit(driver.findElement(locator));
    }

    public String getText(WebElement element){
        return  element.getText();
    }

    public String getText(By locator) {
        if (waitUntilVisible(locator)){
            return getText(driver.findElement(locator));
        } else {
            return null;
        }
    }

    public String getAttribute(WebElement element, String attribute){
        return element.getAttribute(attribute);
    }

    public String getAttribute(By locator, String attribute){
        if (waitUntilVisible(locator)){
            return getAttribute(driver.findElement(locator),attribute);
        } else {
            return null;
        }
    }

    public boolean waitUntilVisible(By locator){
        WebElement until = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return until != null;
    }

    public void waitUntilTextEquals(By locator, String text){
        /*wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                System.out.println("Searching... " + webDriver.findElement(locator).getText() + " ?= "+text);
                return webDriver.findElement(locator).getText().equals(text);
            }
        });*/

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(3, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver,WebElement>() {
            public WebElement apply(WebDriver driver) {
                if (text.equals(getText(locator))) {
                    System.out.println("Searching... " + getText(locator) + " == " + text);
                    return driver.findElement(locator);
                } else {
                    System.out.println("Searching... " + getText(locator) + " != " + text);
                    return null;
                }
            }
        });
    }

    public void navigateToUrl(String url){
        driver.get(url);
    }

    public String getActualPageTitle(){
        return driver.getTitle();
    }

}
