package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rátai Dániel on 2017. 05. 30..
 */
public class StaticPageObject extends PageObject {

    private String url;
    private List<StaticPageElement> elements;

    public StaticPageObject(WebDriver driver, String url) {
        super(driver);
        this.url = url;
        elements = new ArrayList<>();
    }

    public void addElementToTest(String xpath, String property, String expectedValue){
        elements.add(new StaticPageElement(xpath,property,expectedValue));
    }

    public void performStaticPageTest(){
        actionBot.navigateToUrl(url);
        for (int i = 0; i< elements.size(); i++){
            testElement(elements.get(i));
        }
    }

    private void testElement(StaticPageElement element){
        By locator = By.xpath(element.getXpath());
        String result;
        switch (element.getProperty().toUpperCase()){
            case "TEXT":
                result = actionBot.getText(locator);
                break;
            default:
                result = actionBot.getAttribute(locator,element.getProperty());
                break;
        }
        System.out.print("Checking element: ");
        System.out.println(element);
        System.out.println("\""+element.getExpectedValue() + "\" ?= \"" + result+"\"");
        assertEquals(element.getExpectedValue(),result);
    }
}
