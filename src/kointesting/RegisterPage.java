package kointesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Rátai Dániel on 2017. 05. 31..
 */
public class RegisterPage extends PageObject {
    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public void registerWithEmail(String email, String password){
        actionBot.navigateToUrl("https://www.koin.hu/");
        actionBot.click(By.id("signup-with-email-btn"));
        actionBot.retype(By.id("registrationform-surname"),"Teszt");
        actionBot.retype(By.id("registrationform-forename"),"Elek");
        actionBot.retype(By.id("registrationform-email"),email);
        actionBot.retype(By.id("registrationform-pass"),password);
        actionBot.retype(By.id("registrationform-passagain"),password);
        actionBot.click(By.id("registrationform-acceptterms"));
    }
}
